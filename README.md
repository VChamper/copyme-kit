# COPYME Éditions kit

This repository contains the templates and others files needed to make a COPYME Éditions book.

## fonts

COPYME Éditions use Linux Libertine fonts in their book covers.

## logos

COPYME Éditions' logos in black and white variants

## templates

CSS styles, HTML templates, template for a book cover.

## example-book

Example folder of a COPYME Édition book - all the above materials in context.
